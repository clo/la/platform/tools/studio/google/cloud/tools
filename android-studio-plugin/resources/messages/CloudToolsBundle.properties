#
# Copyright (C) 2014 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

application.name.not.set=Application name must be set
create.project.dir.failed=Failed to create project directory
git.project.dir.empty=Failed to find any projects in Git repository
git.project.missing.sample.root=Could not find sample root '{0}' in Git repository
select.project.location=Select project location
select.project.signin=Sign in to Android Studio with your Google account to list your Google Developers Console projects.

sample.import.title=Import Sample
sample.import.progress.title=Importing...
sample.index.downloading=Downloading samples index...
sample.index.download.failed=Failed to download samples index, please check your connection and try again
sample.import.error.title=Sample Import Failed
sample.browser.title=Browse Samples
sample.browser.subtitle=Select a sample to import into Android Studio
sample.browse.source=Browse source in GitHub
sample.browser.please.select=Please select a sample
sample.browser.no.description=No description available
sample.browser.no.preview=No preview available
sample.setup.title=Sample Setup
sample.setup.subtitle=Provide information about your project
sample.default.name=My Imported Sample
sample.copy.to.project.failed=Failed to copy sample into project directory

appengine.gradle.before.run.name=App Engine Gradle builder
appengine.gradle.before.run.description=Sync and build App Engine module
appengine.gradle.before.run.sync.failed=Unable to launch {0} task. Project sync failed with message: {1}
appengine.gradle.before.run.no.facet=Unable to launch {0}, no App Engine Gradle facet found
appengine.gradle.before.run.no.path=Unable to launch {0}, no Gradle path configuration found

appengine.run.no.facet=App Engine Gradle configuration not detected on module, you may need to Sync Project with Gradle
appengine.run.no.gradle.state=App Engine Gradle configuration does not appear to be loaded, please Sync Project with Gradle files before running
appengine.run.invalid.sdk=Invalid App Engine SDK Path
appengine.run.invalid.war=No War Path Specified
appengine.run.invalid.server.address=Server address must contain no spaces
appengine.run.invalid.server.port.range=Invalid port number [valid range : {0} - {1}]
appengine.run.invalid.server.port=Non numeric/invalid port number [valid range : {0} - {1}]
appengine.run.sync.checkbox=Synchronize with build.gradle configuration
appengine.run.disable.check.updates=Disable check for App Engine SDK updates
appengine.run.disable.check.updates.tooltip=Checking this box will prevent the Dev App Server from checking for new \
  versions of the App Engine SDK with the App Engine servers before running. You can \
  also configure this with the "disableUpdateCheck=true" option in your build.gradle

appengine.wizard.android_studio=Android Studio
appengine.wizard.gallery_title=Google Cloud Module
appengine.wizard.gallery_description=Creates a new Google Cloud module.
appengine.wizard.step_title=New Google Cloud Module
appengine.wizard.step_body=Configure your new Google Cloud module
appengine.wizard.module_type_description=<html><body>Check the <a href="{0}">"{1}"</a> \
  documentation for more information about the contents of this backend module, and for \
  detailed instructions about connecting your Android app to this backend.</body></html>
appengine.wizard.please_select_module_type=Please select a module type
appengine.wizard.please_select_module_name=Please enter a module name
appengine.wizard.please_enter_valid_module_name=Please enter a valid module name
appengine.wizard.module_already_exists=Module "{0}" already exists
appengine.wizard.please_enter_package_name=Please enter a package name
appengine.wizard.please_enter_valid_package_name=Please enter a valid package name
appengine.wizard.please_select_client_module=Please select a client module
appengine.wizard.module_type_label=Module type:
appengine.wizard.module_type_label_hint=The module type to generate
appengine.wizard.module_name_label=Module name:
appengine.wizard.module_name_label_hint=The name of your new backend module
appengine.wizard.package_name_label=Package name:
appengine.wizard.package_name_label_hint=The package name to use in your new backend module
appengine.wizard.client_module_label=Client module:
appengine.wizard.client_module_label_hint=The module that contains your Android app
appengine.wizard.prefilled_module_name=backend
appengine.wizard.prefilled_package_name=com.example.myapplication.backend
appengine.wizard.prefilled_user_package_name=com.example.{0}.myapplication.backend
appengine.sdk.invalid=A valid App Engine SDK was not found for module : {0}.
appengine.sdk.gradle.install=Install using Gradle
appengine.sdk.open.gradle.build=Open build.gradle

clonefromgcp.projectid=Cloud Project
clonefromgcp.title=Clone from Google Cloud
clonefromgcp.button=Clone
clonefromgcp.destination.directory.description=Select a parent directory destination directory for clone
clonefromgcp.destination.directory.title=Parent Directory
clonefromgcp.destination.exists.error=The path {0} exists. Repository cannot be cloned to an existing directory.
clonefromgcp.parent.missing.error=The parent path {0} must exist.
clonefromgcp.repository=Cloning source repository {0}
clonefromgcp.failed=Clone failed

uploadtogcp.text=Upload Project to Google Cloud...
uploadtogcp.description=Easily use Google as a source repository.
uploadtogcp.giterror=Error Running Git
uploadtogcp.git.unsupported.message=<html><tt>{0}</tt><br>This version is unsupported, and some plugin functionality could fail to work.<br>The minimal supported version is <em>{1}</em>.</html>
uploadtogcp.alreadyexists=Project is already on Google Cloud Platform
uploadtogcp.selecttext=Select a Google Cloud Project
uploadtogcp.oktext=Upload
uploadtogcp.backgroundtitle=Sharing project on Google Cloud...
uploadtogcp.indicatorinit=Creating empty git repo...
uploadtogcp.failedtocreategit=Failed to create Git Repository
uploadtogcp.addingremote=Adding Google as a remote host...
uploadtogcp.pushingtotgcp=Pushing to Google master...
uploadtogcp.success=Successfully shared project on Google Cloud Platform...
uploadtogcp.addremotefailed=Failed to add remote: {0}. error:{1}
uploadtogcp.addremotefailedtitle=Failed to Add Git Remote
uploadtogcp.initialcommitfailed=Initial commit failed: {0}
uploadtogcp.initialcommitfailedtitle=Initial Git Commit Failed
uploadtogcp.initialpushfailed=Initial push failed.
uploadtogcp.initialpushfailedtitle=Initial Git Push Failed
uploadtogcp.fetching=Fetching from Google remote host...
uploadtogcp.fetchfailed=Failed to Fetch Remote {0}
uploadtogcp.fetchfailedtitle=Fetch Failed.
uploadtogcp.remotenotemptytitle=Error.
uploadtogcp.remotenotempty=The Google Cloud Project "{0}" already has source.

select.user.login=Google Login
select.user.continue=Continue
select.user.manuallogin=Login Manually
select.user.emptytext=Please select a login...
select.user.signin=Sign in to Android Studio with your Google account to select a login.
httpauthprovider.chooselogin=Choose a Google Login to perform this action.
uploadsourceaction.addfiles=Adding files to git...
uploadsourceaction.performingcommit=Performing commit...
uploadsourceaction.addfilestitle=Add Files For Initial Commit

clouddebug.attach.anyway=Attach Anyway
clouddebug.attachtitle=Attach to an Application
clouddebug.stashbuttontext=Stash local changes
clouddebug.attach=Attach
clouddebug.nologin=You must be logged in to perform this action.
clouddebug.noprojectid=Please enter a Project ID.
clouddebug.nomodule=Please select a Module.
clouddebug.readonly=<html><b>Note:</b><i>The Cloud Debugger is read-only and won't suspend nor modify your application.</html>
clouddebug.sourcedoesnotmatch=Your current source does not match what is running on the server.
clouddebug.snapshot.location=Add Cloud snapshot location
clouddebug.adds.snapshot.location=Adds a snapshot location for the cloud debugger
clouddebug.nosnapshots=No shapshot locations have been set.
clouddebug.delete.all=Delete All
clouddebug.remove.all=Remove all pending locations and snapshots?
clouddebug.delete.snapshots=Delete Snapshots
clouddebug.buttondelete=Delete
clouddebug.cancelbutton=Cancel
clouddebug.reactivatesnapshotlocation=Reactivate Snapshot Location
clouddebug.pendingstatus=Pending
clouddebug.moreHTML=<HTML><U>More...</U></HTML>
clouddebug.enabledinfo=The Cloud Debugger takes a single snapshot each time a location is enabled.
clouddebug.errorset=The breakpoint could not be set.
clouddebug.neversuspends=The Cloud Debugger will never suspend any threads.
clouddebug.continuesession=Continue Session
clouddebug.continueanyway=Continue Anyway
clouddebug.shownotifications.option=&Show notifications when not attached.
clouddebug.backgroundinfo=<html>The Cloud Debugger will collect snapshots even when you aren't attached.<br> Notifications will appear when new snapshots are available.</html>
clouddebug.text=Cloud Debug
clouddebug.googleclouddebugger=Google Cloud Debugger
clouddebug.stackat=Stack at {0}
clouddebug.restorestash=Do you want to checkout branch {0}\
 and restore the saved stash?
clouddebug.restorechanges.title=Restore Changes
clouddebug.erroroncheckout=Error checking out branch {0}.  Use VCS->Git->Unstash Changes to restore changes.
clouddebug.unstashmergeconflicts=There are merge conflicts that need to be fixed.
clouddebug.removestashx=Removing stash {0}
clouddebug.fallbackerrormessage=General error
clouddebug.balloonnotification.message=<b>changes detected on {0}</b> <br/>You may <a href\\\="\\\#">continue the debug session</a>.
clouddebug.breakpoint.description=Cloud Snapshot Locations
clouddebug.watchexpressiongrouptitle=Custom Watch Expressions
clouddebug.suspendnotavailable=&Suspend. Not available.
clouddebug.module=&Module
clouddebug.projectlabel=&Project
clouddebug.warninglabel=Warning\:
clouddebug.attachingtext=Attaching...
clouddebug.stopandcontinue=Stop IDE Debugging. The server will continue to watch for breakpoints.
clouddebug.exitdebug=Exit debug mode and continue the debug session later.
clouddebug.errortitle=Cloud Debugger
clouddebug.bad_login_message=The operation failed because you are not logged in.
